CFLAGS?=-Wall -ggdb -O2

gplay_pkgs:=gstreamer-video-1.0 gtk+-2.0 unique-1.0
gplay_cflags:=$(shell pkg-config --cflags $(gplay_pkgs))
gplay_libs:=$(shell pkg-config --libs $(gplay_pkgs))

gplay_objs:=gplay.o

all: gplay

gplay: $(gplay_objs)
	$(CC) $(LDFLAGS) -o $@ $+ $(gplay_libs)

$(gplay_objs): %.o: %.c
	$(CC) $(gplay_cflags) $(CFLAGS) -o $@ -c $<

clean:
	rm -f gplay *.o

install: gplay
	install -d $(DESTDIR)/usr/bin
	install -m 0755 gplay $(DESTDIR)/usr/bin/gplay
	install -d $(DESTDIR)/usr/share/applications
	install -m 0644 gplay.desktop $(DESTDIR)/usr/share/applications/gplay.desktop

uninstall:
	rm -f $(DESTDIR)/usr/bin/gplay $(DESTDIR)/usr/share/applications/gplay.desktop
