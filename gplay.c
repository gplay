#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdkx.h>
#include <gst/gst.h>
#include <gst/video/videooverlay.h>
#include <unique/unique.h>

static GtkWindow *main_win =  NULL;
static GtkWidget *video_widget;
static GtkScale *slider_widget;
static GtkToolItem *play_tool, *pause_tool;

static GtkFileChooserDialog *open_file_chooser = NULL;

static GCancellable *icon_cancellable = NULL;

static GstElement *playbin = NULL;
static GstState play_state;

static gboolean window_size_updated = FALSE;
static gint video_width = 0, video_height = 0;
static gint64 media_duration = 0;

static gulong slider_changed_connection_id = 0;
static gulong position_update_source = 0;

static void play_media()
{
	gst_element_set_state(playbin, GST_STATE_PLAYING);
}

static void pause_media()
{
	gst_element_set_state(playbin, GST_STATE_PAUSED);
}

static void seek_relative_media(gdouble seconds)
{
	gint64 current, target;
	GstState target_state = play_state == GST_STATE_PLAYING ? GST_STATE_PLAYING : GST_STATE_PAUSED;

	gst_element_set_state(playbin, GST_STATE_PAUSED);

	if (gst_element_query_position(playbin, GST_FORMAT_TIME, &current)) {
		target = current + (gint64)(seconds * GST_SECOND);
	} else {
		g_debug("could not query current media position");
		return;
	}

	if (target < 0) target = 0;

	gst_element_seek_simple(playbin, GST_FORMAT_TIME,
	                        GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
	                        target);

	gst_element_set_state(playbin, target_state);
}

static void update_position_ui()
{
	gint64 current;

	g_return_if_fail(play_state >= GST_STATE_PAUSED);

	if (!GST_CLOCK_TIME_IS_VALID(media_duration)) {
		if (gst_element_query_duration(playbin, GST_FORMAT_TIME, &media_duration)) {
			const gdouble seconds = (gdouble)media_duration / GST_SECOND;
			g_debug("got media duration: %f seconds", seconds);
			gtk_range_set_range(GTK_RANGE(slider_widget), 0, seconds);
		} else {
			g_debug("could not query current media duration");
			return;
		}
	}

	if (gst_element_query_position(playbin, GST_FORMAT_TIME, &current)) {
		const gdouble seconds = (gdouble)current / GST_SECOND;
		g_signal_handler_block(slider_widget, slider_changed_connection_id);
		gtk_range_set_value(GTK_RANGE(slider_widget), seconds);
		g_signal_handler_unblock(slider_widget, slider_changed_connection_id);
	} else {
		g_debug("could not query current media position");
		return;
	}

	gtk_widget_set_sensitive(GTK_WIDGET(slider_widget), TRUE);
}

static void reset_position_ui()
{
	gtk_widget_set_sensitive(GTK_WIDGET(slider_widget), FALSE);
	gtk_range_set_range(GTK_RANGE(slider_widget), 0.0, 1.0);
	gtk_range_set_value(GTK_RANGE(slider_widget), 0.0);
	media_duration = GST_CLOCK_TIME_NONE;
}

static void open_icon_ready(GObject *source, GAsyncResult *res, gpointer user_data)
{
	GFile *file = G_FILE(source);
	GError *error = NULL;
	GFileInfo *info = g_file_query_info_finish(file, res, &error);
	GdkPixbuf *icon_pixbuf = NULL;

	if (info) {
		GIcon *icon = g_file_info_get_icon(info);
		if (icon) {
			GtkIconTheme *theme = gtk_icon_theme_get_default();
			GtkIconInfo *info = gtk_icon_theme_lookup_by_gicon(theme, icon, GTK_ICON_SIZE_DIALOG, 0);
			if (info) {
				icon_pixbuf = gtk_icon_info_load_icon(info, &error);
				if (!icon_pixbuf) {
					gchar *uri = g_file_get_uri(file);
					g_debug("Could not load icon for file '%s': %s", uri, error->message);
					g_free(uri);
					g_error_free(error);
				}
			}
		}

		g_object_unref(info);
	} else {
		gchar *uri = g_file_get_uri(file);
		g_debug("Could not get icon for file '%s': %s", uri, error->message);
		g_free(uri);
		g_error_free(error);
	}

	gtk_window_set_icon(main_win, icon_pixbuf);
}

static void open_media(const char *uri)
{
	g_debug("opening '%s'", uri);

	gst_element_set_state(playbin, GST_STATE_NULL);

	window_size_updated = FALSE;
	video_width = 0;
	video_height = 0;
	media_duration = GST_CLOCK_TIME_NONE;

	if (icon_cancellable) {
		g_cancellable_cancel(icon_cancellable);
		g_clear_object(&icon_cancellable);
	}

	g_object_set(playbin, "uri", uri, NULL);

	// Set window title from uri
	gchar *filename = g_filename_from_uri(uri, NULL, NULL);
	if (filename) {
		gchar *basename = g_filename_display_basename(filename);
		gtk_window_set_title(main_win, basename);
		g_free(basename);
		g_free(filename);
	} else {
		gtk_window_set_title(main_win, g_get_application_name());
	}

	// Set window icon from file
	GFile *file = g_file_new_for_uri(uri);
	icon_cancellable = g_cancellable_new();

	g_file_query_info_async(file, G_FILE_ATTRIBUTE_STANDARD_ICON, G_FILE_QUERY_INFO_NONE,
	                        G_PRIORITY_LOW, icon_cancellable,
	                        open_icon_ready, NULL);
	g_object_unref(file);

	// Add to recent manager
	GtkRecentManager *recents = gtk_recent_manager_get_default();
	gtk_recent_manager_add_item(recents, uri);

	gst_element_set_state(playbin, GST_STATE_PAUSED);
}

static void open_file_chooser_handle_response(GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_ACCEPT) {
		gchar *filename = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(open_file_chooser));

		open_media(filename);

		g_free(filename);

		gst_element_set_state(playbin, GST_STATE_PLAYING);
	}

	gtk_widget_destroy(GTK_WIDGET(open_file_chooser));
	open_file_chooser = 0;
}

static void menu_handle_open(GtkMenuItem *item, gpointer user_data)
{
	if (open_file_chooser) {
		gtk_window_present_with_time(GTK_WINDOW(open_file_chooser), gtk_get_current_event_time());
		return;
	}

	open_file_chooser = GTK_FILE_CHOOSER_DIALOG(gtk_file_chooser_dialog_new(
	                                   _("Open media"), main_win, GTK_FILE_CHOOSER_ACTION_OPEN,
	                                   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
	                                   NULL));
	g_signal_connect(open_file_chooser, "response",
	                 G_CALLBACK(open_file_chooser_handle_response), NULL);

	gtk_widget_show_all(GTK_WIDGET(open_file_chooser));
}

static void slider_widget_handle_value_changed(GtkRange *range, gpointer user_data) {
	if (play_state < GST_STATE_PAUSED) return;

	gdouble value = gtk_range_get_value(range);
	gst_element_seek_simple(playbin, GST_FORMAT_TIME,
	                        GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
	                        (gint64)(value * GST_SECOND));
}

static void video_widget_handle_realize(GtkWidget *widget, gpointer user_data)
{
	GdkWindow *window = gtk_widget_get_window(widget);
	g_return_if_fail(window);

	gst_video_overlay_set_window_handle(GST_VIDEO_OVERLAY(playbin),
	                                    GDK_WINDOW_XID(window));
}

static gboolean video_widget_handle_expose(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
	GdkWindow *window = gtk_widget_get_window(widget);
	g_return_if_fail(window);

	if (video_width == 0 || video_height == 0) {
		gdk_window_clear(window);
	}

	return TRUE;
}

static gboolean second_tick(gpointer user_data)
{
	update_position_ui();

	return G_SOURCE_CONTINUE;
}

static void bus_handle_error(GstBus *bus, GstMessage *msg, gpointer user_data)
{
	GError *error = NULL;
	gchar *debug_info;
	gst_message_parse_error(msg, &error, &debug_info);
	g_warning("Error received from element '%s': %s",
	          GST_OBJECT_NAME(GST_MESSAGE_SRC(msg)),
	          error->message);
	if (debug_info) {
		g_debug("Debug info: %s", debug_info);
	}
	g_error_free(error);
	g_free(debug_info);
}

static void bus_handle_eos(GstBus *bus, GstMessage *msg, gpointer user_data)
{
	g_debug("End-Of-Stream reached");
	// Go back to the initial position
	gst_element_set_state(playbin, GST_STATE_PAUSED);
	gst_element_seek_simple(playbin, GST_FORMAT_TIME,
	                        GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
	                        0);
}

static void bus_handle_state_changed(GstBus *bus, GstMessage *msg, gpointer user_data)
{
	GstState old_state, new_state, pending_state;
	gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state);
	if (GST_MESSAGE_SRC(msg) == GST_OBJECT(playbin)) {
		g_debug("playbin state change: %s", gst_element_state_get_name(new_state));

		play_state = new_state;

		gboolean update_size = FALSE, reset_size = FALSE;
		switch(new_state) {
		case GST_STATE_PLAYING:
			gtk_widget_hide(GTK_WIDGET(play_tool));
			gtk_widget_show(GTK_WIDGET(pause_tool));
			gtk_widget_set_sensitive(GTK_WIDGET(play_tool), TRUE);
			update_size = !window_size_updated;
			if (!position_update_source) {
				position_update_source = g_timeout_add_seconds(1, second_tick, NULL);
			}
			break;
		case GST_STATE_PAUSED:
			gtk_widget_hide(GTK_WIDGET(pause_tool));
			gtk_widget_show(GTK_WIDGET(play_tool));
			gtk_widget_set_sensitive(GTK_WIDGET(play_tool), TRUE);
			update_size = !window_size_updated;
			if (position_update_source) {
				g_source_remove(position_update_source);
				position_update_source = 0;
			}
			update_position_ui();
			break;
		case GST_STATE_READY:
			gtk_widget_hide(GTK_WIDGET(pause_tool));
			gtk_widget_show(GTK_WIDGET(play_tool));
			gtk_widget_set_sensitive(GTK_WIDGET(play_tool), TRUE);
			if (position_update_source) {
				g_source_remove(position_update_source);
				position_update_source = 0;
			}
			reset_size = TRUE;
			reset_position_ui();
			break;
		default:
			gtk_widget_hide(GTK_WIDGET(pause_tool));
			gtk_widget_show(GTK_WIDGET(play_tool));
			gtk_widget_set_sensitive(GTK_WIDGET(play_tool), FALSE);
			if (position_update_source) {
				g_source_remove(position_update_source);
				position_update_source = 0;
			}
			reset_size = TRUE;
			reset_position_ui();
			break;
		}


		if (update_size) {
			GstPad *pad;
			g_signal_emit_by_name(playbin, "get-video-pad", 0, &pad);
			if (pad) {
				GstCaps *caps = gst_pad_get_current_caps(pad);
				if (caps) {
					GstStructure *s = gst_caps_get_structure(caps, 0);
					gst_structure_get_int(s, "width", &video_width);
					gst_structure_get_int(s, "height", &video_height);
					g_debug("video size %dx%d", video_width, video_height);

					GtkAllocation allocation, window_allocation;
					gtk_widget_get_allocation(video_widget, &allocation);
					gtk_widget_get_allocation(GTK_WIDGET(main_win), &window_allocation);

					gint window_width, window_height;
					gtk_window_get_default_size(main_win, &window_width, &window_height);

					window_width = MAX(window_allocation.width - allocation.width + video_width, window_width);
					window_height = MAX(window_allocation.height - allocation.height + video_height, window_height);

					g_debug("resizing window to %dx%d", window_width, window_height);
					gtk_window_resize(main_win, window_width, window_height);

					window_size_updated = TRUE;

					gst_caps_unref(caps);
				}
				gst_object_unref(pad);
			}
		} else if (reset_size) {
			gint width, height;
			gtk_window_get_default_size(main_win, &width, &height);
			gtk_window_resize(main_win, width, height);
		}
	}
}

static void bus_handle_duration_changed(GstBus *bus, GstMessage *msg, gpointer user_data)
{
	g_debug("duration changed");
	media_duration = GST_CLOCK_TIME_NONE;
}

static gboolean main_win_handle_key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	switch (event->keyval) {
	case GDK_KEY_space:
		switch (play_state) {
		case GST_STATE_READY:
		case GST_STATE_PAUSED:
			gst_element_set_state(playbin, GST_STATE_PLAYING);
			break;
		case GST_STATE_PLAYING:
			gst_element_set_state(playbin, GST_STATE_PAUSED);
			break;
		default:
			break;
		}

		return TRUE;
	case GDK_KEY_Left:
		if (play_state < GST_STATE_PAUSED) return TRUE;
		if (event->state & GDK_CONTROL_MASK) {
			seek_relative_media(-30.0);
		} else if (event->state & GDK_SHIFT_MASK) {
			seek_relative_media(-1.0);
		} else {
			seek_relative_media(-10.0);
		}
		return TRUE;
	case GDK_KEY_Right:
		if (play_state < GST_STATE_PAUSED) return TRUE;
		if (event->state & GDK_CONTROL_MASK) {
			seek_relative_media(+30.0);
		} else if (event->state & GDK_SHIFT_MASK) {
			seek_relative_media(+1.0);
		} else {
			seek_relative_media(+10.0);
		}
		return TRUE;
	case GDK_KEY_Down:
		if (play_state < GST_STATE_PAUSED) return TRUE;
		if (event->state & GDK_CONTROL_MASK) {
			seek_relative_media(-10.0 * 60.0);
		} else {
			seek_relative_media(-1.0 * 60.0);
		}
		return TRUE;
	case GDK_KEY_Up:
		if (play_state < GST_STATE_PAUSED) return TRUE;
		if (event->state & GDK_CONTROL_MASK) {
			seek_relative_media(+10.0 * 60.0);
		} else {
			seek_relative_media(+1.0 * 60.0);
		}
		return TRUE;

	case GDK_KEY_Escape:
		gtk_main_quit();
		return TRUE;

	default:
		return FALSE;
	}
}

static UniqueResponse app_handle_message_received(UniqueApp *app, gint command, UniqueMessageData *data, guint timestamp, gpointer user_data)
{
	switch (command) {
	case UNIQUE_ACTIVATE:
		gtk_window_present_with_time(main_win, timestamp);
		return UNIQUE_RESPONSE_OK;
	case UNIQUE_OPEN:
		if (data) {
			gchar **uris = unique_message_data_get_uris(data);
			if (uris && uris[0]) {
				open_media(uris[0]);
				play_media();
				gtk_window_present_with_time(main_win, timestamp);
				return UNIQUE_RESPONSE_OK;
			}
		}

		/* Fallthrough */
	default:
		return UNIQUE_RESPONSE_FAIL;
	}
}

static void create_ui()
{
	main_win = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	g_signal_connect(main_win, "key-press-event",
	                 G_CALLBACK(main_win_handle_key_press), NULL);
	g_signal_connect(main_win, "delete-event",
	                 G_CALLBACK(gtk_main_quit), NULL);

	GtkBox *vbox = GTK_BOX(gtk_vbox_new(FALSE, 0));
	gtk_container_add(GTK_CONTAINER(main_win), GTK_WIDGET(vbox));

	GtkMenuBar *menubar = GTK_MENU_BAR(gtk_menu_bar_new());
	gtk_box_pack_start(vbox, GTK_WIDGET(menubar), FALSE, FALSE, 0);

	GtkMenuShell *file_menu = GTK_MENU_SHELL(gtk_menu_new());

	GtkMenuItem *file_item = GTK_MENU_ITEM(gtk_menu_item_new_with_mnemonic(_("_File")));
	gtk_menu_item_set_submenu(file_item, GTK_WIDGET(file_menu));
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), GTK_WIDGET(file_item));

	GtkWidget *open_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN, NULL);
	g_signal_connect(open_item, "activate",
	                 G_CALLBACK(menu_handle_open), NULL);
	gtk_menu_shell_append(file_menu, open_item);

	gtk_menu_shell_append(file_menu, gtk_separator_menu_item_new());

	GtkWidget *quit_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT, NULL);
	g_signal_connect(quit_item, "activate",
	                 G_CALLBACK(gtk_main_quit), NULL);
	gtk_menu_shell_append(file_menu, quit_item);

	video_widget = gtk_drawing_area_new();
	gtk_widget_set_double_buffered(GTK_WIDGET(video_widget), FALSE);
	g_signal_connect(video_widget, "realize",
	                 G_CALLBACK(video_widget_handle_realize), NULL);
	g_signal_connect(video_widget, "expose-event",
	                 G_CALLBACK(video_widget_handle_expose), NULL);
	gtk_box_pack_start(vbox, video_widget, TRUE, TRUE, 0);

	GtkToolbar *toolbar = GTK_TOOLBAR(gtk_toolbar_new());
	gtk_widget_set_can_focus(GTK_WIDGET(toolbar), FALSE);
	gtk_box_pack_start(vbox, GTK_WIDGET(toolbar), FALSE, FALSE, 0);

	play_tool = gtk_tool_button_new_from_stock(GTK_STOCK_MEDIA_PLAY);
	gtk_widget_set_can_focus(GTK_WIDGET(play_tool), FALSE);
	g_signal_connect(play_tool, "clicked",
	                 G_CALLBACK(play_media), NULL);
	gtk_toolbar_insert(toolbar, play_tool, -1);

	pause_tool = gtk_tool_button_new_from_stock(GTK_STOCK_MEDIA_PAUSE);
	gtk_widget_set_can_focus(GTK_WIDGET(pause_tool), FALSE);
	g_signal_connect(pause_tool, "clicked",
	                 G_CALLBACK(pause_media), NULL);
	gtk_toolbar_insert(toolbar, pause_tool, -1);

	slider_widget = GTK_SCALE(gtk_hscale_new_with_range(0, 100, 1));
	gtk_widget_set_can_focus(GTK_WIDGET(slider_widget), FALSE);
	gtk_scale_set_draw_value(slider_widget, FALSE);
	slider_changed_connection_id = g_signal_connect(slider_widget, "value-changed",
	                                                G_CALLBACK(slider_widget_handle_value_changed), NULL);
	GtkToolItem *slider_tool = gtk_tool_item_new();
	gtk_tool_item_set_expand(slider_tool, TRUE);
	gtk_container_add(GTK_CONTAINER(slider_tool), GTK_WIDGET(slider_widget));
	gtk_toolbar_insert(toolbar, slider_tool, -1);

	const GtkTargetEntry drop_targets[] = {{ "text/uri-list", 0, 0 }};
	gtk_drag_dest_set(GTK_WIDGET(main_win), GTK_DEST_DEFAULT_ALL,
	                  drop_targets, G_N_ELEMENTS(drop_targets),
	                  GDK_ACTION_DEFAULT);

	gtk_window_set_default_size(main_win, 350, 20);

	gtk_widget_show_all(GTK_WIDGET(vbox));
	gtk_widget_hide(GTK_WIDGET(pause_tool));
}

static void create_gst()
{
	playbin = gst_element_factory_make("playbin", "playbin");
	g_return_if_fail(playbin);

	GstBus *bus = gst_element_get_bus(playbin);
	gst_bus_add_signal_watch(bus);
	g_signal_connect(bus, "message::eos",
	                 G_CALLBACK(bus_handle_eos), NULL);
	g_signal_connect(bus, "message::error",
	                 G_CALLBACK(bus_handle_error), NULL);
	g_signal_connect(bus, "message::state-changed",
	                 G_CALLBACK(bus_handle_state_changed), NULL);
	g_signal_connect(bus, "message::duration-changed",
	                 G_CALLBACK(bus_handle_duration_changed), NULL);
	gst_object_unref(bus);
}

int main(int argc, char **argv)
{
	GOptionContext *ctx = g_option_context_new(_(" - simple gtk media player"));
	gchar **filenames = NULL;
	gboolean new_instance = FALSE;
	const GOptionEntry entries[] = {
	    { "new", 'n', 0, G_OPTION_ARG_NONE, &new_instance, _("Open in a new instance"), NULL },
	    { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &filenames, _("URI of media to play"), NULL },
	    { NULL }
	};
	GError *error = NULL;

	g_set_application_name("Gplay");

	g_option_context_add_main_entries(ctx, entries, NULL);
	g_option_context_add_group(ctx, gst_init_get_option_group());
	g_option_context_add_group(ctx, gtk_get_option_group(TRUE));

	if (!g_option_context_parse(ctx, &argc, &argv, &error)) {
		g_printerr("Invalid usage: %s", error->message);
		g_error_free(error);
		return EXIT_FAILURE;
	}

	g_option_context_free(ctx);

	UniqueApp *app = unique_app_new("com.javispedro.gplay", NULL);

	gchar *uri = NULL;
	if (filenames && filenames[0]) {
		const char *file = filenames[0];
		if (gst_uri_is_valid(file)) {
			uri = g_strdup(file);
		} else {
			uri = gst_filename_to_uri(file, &error);
			if (!uri) {
				g_printerr("Invalid filename '%s':%s", file, error->message);
				g_error_free(error);
				return EXIT_FAILURE;
			}
		}
	}

	if (unique_app_is_running(app) & !new_instance && uri) {
		UniqueMessageData *data = unique_message_data_new();
		gchar *uris[] = { uri, NULL };

		unique_message_data_set_uris(data,  uris);

		if (unique_app_send_message(app, UNIQUE_OPEN, data) == UNIQUE_RESPONSE_OK) {
			g_free(uri);
			return EXIT_SUCCESS;
		}
	}

	g_signal_connect(app, "message-received",
	                 G_CALLBACK(app_handle_message_received), NULL);

	create_ui();
	create_gst();

	if (uri) {
		open_media(uri);
		play_media();
		g_free(uri);
	}

	gtk_widget_show(GTK_WIDGET(main_win));
	gtk_main();

	return EXIT_SUCCESS;
}
